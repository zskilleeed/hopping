﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSceneController : MonoBehaviour
{

    public void restartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

    }

    public void play()
    {
        SceneManager.LoadScene("SampleScene");

    }

    public void menu()
    {

        SceneManager.LoadScene("MenuScene");

    }

    public void creditos()
    {
        SceneManager.LoadScene("CreditsScene");

    }

    public void controls()
    {
        SceneManager.LoadScene("ControlsScene");


    }

    public void exit() {

            Application.Quit();

        
    }
}
