﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PointsController : MonoBehaviour
{

    public Text pointsTxt;
    public Text deadPointsTxt;
    public Text pointsTextTxt;
    public Text highscoreText;

    private int highscore;
    private static int points;

    void Start()
    {
        points = 0;
        enableTexts();
        
        highscore = PlayerPrefs.GetInt("highscore");
         setPointsText();
    }


    
    public void addPoints()
    {
        points += 200;
        if(points > highscore)
        {
            highscore = points;
            PlayerPrefs.SetInt("highscore", highscore);
        } 
        setPointsText();
    }
    void setPointsText()
    {
        pointsTxt.text = points.ToString();
        deadPointsTxt.text = points.ToString();
        highscoreText.text = highscore.ToString();
    }

    public int getPoints()
    {
        return points;
    }
    void enableTexts()
    {
        pointsTxt.enabled = true;
        pointsTextTxt.enabled = true;
    }


    public void disableTexts()
    {
        pointsTxt.enabled = false;
        pointsTextTxt.enabled = false;
    }
}
