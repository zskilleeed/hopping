﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyIA : MonoBehaviour
{

    public GameObject bullet;
    private int shootRotationSpeed = 500;
    private GameObject player;
    public bool moving;
    int shootCountdown;
    int rand;
    int once = 0;

    void Start()
    {
        moving = true;
        shootCountdown = 0;
        player = GameObject.FindGameObjectWithTag("Player");
        rand = Random.Range(-8, 8);
        InvokeRepeating("shoot", 0, 1.5f);
    }

    // Update is called once per frame
    void Update()
    {
        move();
        this.gameObject.transform.Rotate(new Vector3(0, 0, shootRotationSpeed * Time.deltaTime));
    }

    void shoot()
    {


        if (!moving)
        {


            Instantiate(bullet, this.gameObject.transform.position, new Quaternion(0, 0, 0, 0));


        }

    }

    void move()
    {
        if (moving)
        {
            this.gameObject.transform.position = Vector2.MoveTowards(this.gameObject.transform.position, new Vector2(rand, this.gameObject.transform.position.y), 30 * Time.deltaTime);
            if (this.gameObject.transform.position.x == rand) moving = false;

        }


    }




}
