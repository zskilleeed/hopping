﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCollision : MonoBehaviour
{

    public bool hide = false;
    public bool isHidded;
    private float initPos = -3.064757f;
    private float hidePos = -7;
    public float movementSpeed;
    public GroundController gameManager;
    private int counter;
    void Start()
    {
        counter = 0;
        
        isHidded = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (hide)
        {
            hideGround();
        }
        else
        {
            unhideGround();
        }

    }

    public void hideGround()
    {
        if (this.gameObject.transform.position.y > hidePos)
        {
            counter = 0;
            this.gameObject.transform.position = Vector3.MoveTowards(
                       this.gameObject.transform.position,
                       new Vector3(this.gameObject.transform.position.x, hidePos, 0),
                       movementSpeed * Time.deltaTime
                   );
        }
        else
        {
            isHidded = true;
            if (counter == 0)
            {
                if (gameManager.getTotalHideGrounds() >= 0)
                {
                    gameManager.addHideGround();
                    counter++;
                }

            }

        }

    }

    public void unhideGround()
    {
        if (this.gameObject.transform.position.y < initPos)
        {
            counter = 0;
            this.gameObject.transform.position = Vector3.MoveTowards(
                       this.gameObject.transform.position,
                       new Vector3(this.gameObject.transform.position.x, initPos, 0),
                       movementSpeed * Time.deltaTime
                   );
        }
        else
        {
            isHidded = false;
            if (counter == 0)
            {
                if (gameManager.getTotalHideGrounds() > 0)
                {
                    gameManager.removeHideGround();
                    counter++;
                }
            }
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        
            if (other.gameObject.tag.Equals("Player"))
            {

                
                gameManager.hideRandomGround();
                

            }
    }
}
