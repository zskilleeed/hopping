﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundController : MonoBehaviour
{

    private  int totalHideGrounds;
    private  int totalGrounds;
    private  GameObject[] allGroundsList;
    void Start()
    {
        totalHideGrounds = 0;
        allGroundsList = GameObject.FindGameObjectsWithTag("Ground");
        totalGrounds = allGroundsList.Length;
        Invoke("hideRandomGrounds", 1.2f);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public  void addHideGround()
    {
        totalHideGrounds++;
    }

    public  void removeHideGround()
    {
        totalHideGrounds--;
    }

    public  int getTotalHideGrounds()
    {
        return totalHideGrounds;
    }

    public void hideRandomGrounds()
    {
        List<int> selectedNums = new List<int>();
        int random = 0;
        for (int i = 0; i < totalGrounds / 3; i++)
        {
            do random = Random.Range(0, totalGrounds);
            while (selectedNums.Contains(random));

            selectedNums.Add(random);

        }

        for (int i = 0; i < selectedNums.Count; i++)
        {
            allGroundsList[selectedNums[i]].GetComponent<GroundCollision>().hide = true;
      
        }
    }

    public void unhideRandomGround()
    {
        int random = 0;
    
            do random = Random.Range(0, totalGrounds);
            while (allGroundsList[random].GetComponent<GroundCollision>().hide == false);

            allGroundsList[random].GetComponent<GroundCollision>().hide = false;
        
    }

    public void hideRandomGround()
    {
        int random = 0;
        for (int i = 0; i < 2; i++)
        {
            do random = Random.Range(0, totalGrounds);
            while (allGroundsList[random].GetComponent<GroundCollision>().hide == true);

            allGroundsList[random].GetComponent<GroundCollision>().hide = true;
         
            unhideRandomGround();
        }

    }
}
