﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDestroy : MonoBehaviour
{

    int countdown;
    GameObject player;
    Vector2 moveDirection;
    int speed = 600;
    void Start()
    {
        countdown = 0;
        player = GameObject.FindGameObjectWithTag("Player");
        if (player != null)
        {
            moveDirection = (player.transform.position - transform.position).normalized * speed * Time.deltaTime;
            this.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(moveDirection.x, moveDirection.y);
        }


    }

    // Update is called once per frame
    void Update()
    {
        countdown++;
        if (countdown >= 200)
        {
            Destroy(this.gameObject);
        }
    }
}
