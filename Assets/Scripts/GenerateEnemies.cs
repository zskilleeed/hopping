﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateEnemies : MonoBehaviour
{
    public GameObject enemy;
    private GameObject player;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        InvokeRepeating("generateEnemy", 3, 2.2f);
    }

    // Update is called once per frame
   
    void generateEnemy()
    {
        if(player != null)
        {
        Instantiate(enemy, this.gameObject.transform.position, new Quaternion(0, 0, 0, 0));

        }
    }
}
