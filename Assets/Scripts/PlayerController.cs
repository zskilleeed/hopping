﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{

    enum DIRECTIONS { NONE, RIGHT, LEFT };
    private float movementSpeed = 7f;
    private float dashSpeed = 150f;
    private int counter = 0;
    private Rigidbody2D rb;
    private DIRECTIONS direction;
    private int dashCounter;
    public GameObject cam;
    Animator animator;
    private bool dashing;
    public GameObject deadParticles;
    public GameObject gameManager;
    public GameObject deadScene;
    public bool alive;
    void Start()
    {
        rb = this.gameObject.GetComponent<Rigidbody2D>();
        dashCounter = 0;
        alive = true;
        cam = GameObject.FindGameObjectWithTag("MainCamera");
        animator = cam.GetComponent<Animator>();
        dashing = false;

    }

    // Update is called once per frame
    void Update()
    {

        getInput();
        move();
        dashCountdown();
    }

    public void move()
    {
        switch (direction)
        {
            default:
                break;

            case DIRECTIONS.LEFT:
                transform.Translate(-movementSpeed * Time.deltaTime, 0, 0);
                break;

            case DIRECTIONS.RIGHT:
                transform.Translate(movementSpeed * Time.deltaTime, 0, 0);
                break;

        }

    }

    public void getInput()
    {
        if (Input.GetKey(KeyCode.A))
        {
            direction = DIRECTIONS.LEFT;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            direction = DIRECTIONS.RIGHT;
        }
        else
        {
            direction = DIRECTIONS.NONE;
        }

        //DASH
        if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.Space))
        {
            dash("up_left");
        }
        else if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.Space))
        {
            dash("up_right");
        }
        else if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.Space))
        {
            dash("right");
        }
        else if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.Space))
        {
            dash("left");
        }
        else if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.Space))
        {
            dash("up");
        }


    }


    public void dash(string dir)
    {
        if (dir.Equals("up_left"))
        {
            if (dashCounter == 0)
            {
                if (transform.position.x > -7f)
                {
                    transform.Translate(-dashSpeed * Time.deltaTime, dashSpeed * Time.deltaTime, 0);
                    dashCounter = 1;
                    dashing = true;
                    animator.SetBool("dashing", true);
                    this.gameObject.GetComponent<AudioSource>().Play();

                }

            }
        }
        else if (dir.Equals("up_right"))
        {
            if (dashCounter == 0)
            {
                if (transform.position.x < 7f)
                {
                    transform.Translate(dashSpeed * Time.deltaTime, dashSpeed * Time.deltaTime, 0);
                    dashCounter = 1;
                    dashing = true;
                    animator.SetBool("dashing", true);
                    this.gameObject.GetComponent<AudioSource>().Play();

                }
            }
        }
        else if (dir.Equals("right"))
        {
            if (dashCounter == 0)
            {
                if (transform.position.x < 7f)
                {
                    transform.Translate(dashSpeed * Time.deltaTime, 0, 0);
                    dashCounter = 1;
                    dashing = true;
                    animator.SetBool("dashing", true);
                    this.gameObject.GetComponent<AudioSource>().Play();

                }
            }
        }
        else if (dir.Equals("left"))
        {
            if (dashCounter == 0)
            {
                if (transform.position.x > -7f)
                {
                    transform.Translate(-dashSpeed * Time.deltaTime, 0, 0);
                    dashCounter = 1;
                    dashing = true;
                    animator.SetBool("dashing", true);
                    this.gameObject.GetComponent<AudioSource>().Play();

                }
            }
        }
        else if (dir.Equals("up"))
        {
            if (dashCounter == 0)
            {
                transform.Translate(0, dashSpeed * Time.deltaTime, 0);
                dashCounter = 1;
                dashing = true;
                animator.SetBool("dashing", true);
                this.gameObject.GetComponent<AudioSource>().Play();

            }
        }
    }

    public void dashCountdown()
    {
        if (dashCounter >= 1)
        {
            dashCounter++;
            if (dashCounter >= 10)
            {
                dashing = false;

            }
            if (dashCounter >= 20)
            {
                animator.SetBool("dashing", false);

            }
            if (dashCounter >= 30)
            {
                dashCounter = 0;
            }
        }
    }
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag.Equals("Ground"))
        {
            jump();
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.tag.Equals("Ground"))
        {
            counter = 0;
        }
    }

    public void jump()
    {
        if (counter == 0)
        {
            rb.AddForce(new Vector2(0, 400));
            counter++;
        }
    }


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Enemy")
        {
            if (dashing)
            {
                gameManager.GetComponent<AudioSource>().Play();
                Destroy(other.gameObject);
                Instantiate(deadParticles, this.gameObject.transform.position, this.gameObject.transform.rotation);
                gameManager.GetComponent<PointsController>().addPoints();
            }
        }
        if (other.tag == "Bullet" || other.tag == "Spike")
        {
            if (!dashing)
            {
                dead();
            }
        }
    }

    public void dead()
    {
        gameManager.GetComponent<AudioSource>().Play();
        alive = false;
        animator.SetBool("dashing", false);
        Instantiate(deadParticles, this.gameObject.transform.position, this.gameObject.transform.rotation);
        Destroy(this.gameObject);
        deadScene.transform.position = deadScene.transform.parent.position;
        gameManager.GetComponent<PointsController>().disableTexts();
    }




}
